(require 's)
(require 'subr-x)
(require 'cl-lib)
(require 'url)
(require 'url-util)
(require 'crm)
(require 'rx)

(require 'org-ref-arxiv)

;; overrides the function in `org-ref-arxiv' to do not alter author names
(defun arxiv-bibtexify-authors (authors)
  (s-join " and " authors))

(defconst bibtex-journal-title-abbrevs-file
  (expand-file-name "journal-title-abbrevs.txt" (file-name-directory load-file-name)))

(defvar bibtex-journal-title-abbrevs '())

(defun bibtex-lookup-journal-title-abbrev (title)
  (if (string= title "CoRR")
      title
    (unless bibtex-journal-title-abbrevs
      (with-temp-buffer
	(insert-file-contents bibtex-journal-title-abbrevs-file)
	(while (re-search-forward "^\\(.+\\)\t\\(.+\\)$" nil t)
	  (push (cons (match-string 1) (match-string 2)) bibtex-journal-title-abbrevs)))
      ;; sorting is not necessary but nice
      (sort bibtex-journal-title-abbrevs (lambda (a b) (string< (car a) (car b)))))
    ;; lookup
    (cdr (assoc title bibtex-journal-title-abbrevs))))

(defconst bibtex-abbreviso-api
  "https://abbreviso.toolforge.org/a/")

(defun bibtex-abbrev (title)
  (let ((url-request-method "GET"))
    (with-current-buffer (url-retrieve-synchronously (concat bibtex-abbreviso-api title))
      (buffer-substring (1+ url-http-end-of-headers) (point-max)))))

(defun bibtex-journal-title-abbrev (title &optional fallback)
  (or (bibtex-lookup-journal-title-abbrev title)
      (and fallback (bibtex-abbrev title))))

(defun bibtex-autokey-get-journal (&optional abbrev)
  (let ((journal (bibtex-autokey-get-field "journal")))
    (when journal
      ;; drop everything after a column
      (setq journal (replace-regexp-in-string ":\\(?:.\\|\n\\)*\\'" "" journal t))
      (when abbrev
        (setq journal (bibtex-journal-title-abbrev journal t)))
      (downcase (replace-regexp-in-string "[^[:word:]]" "" journal)))))

(defvar bibtex-autokey-separator "-")

(defun bibtex-generate-autokey ()
  "Generate automatically a key for a BibTeX entry.
Overrides the function defined in bibtex.el."

  (let ((names (bibtex-autokey-get-names))
	(year (bibtex-autokey-get-year))
	(journal (bibtex-autokey-get-journal t))
	(volume (bibtex-autokey-get-field "volume"))
	;; keep only first page number in pages range
	(pages (bibtex-autokey-get-field "pages" '(("-.*\\'" . ""))))
	(archive (downcase (bibtex-autokey-get-field "archivePrefix")))
	;; replace dots with dashes
	(eprint (bibtex-autokey-get-field "eprint" '(("\\." . "-"))))
	(sep bibtex-autokey-separator))
    (if (not (string-empty-p archive))
	(concat names sep archive sep eprint sep year)	     
      (concat names sep journal sep volume sep pages sep year))))

(require 'bibtex)

(defun url-to-doi (url)
  "It is sometime possible to directly map an URL to a doi
  avoiding dowloading the article webpage to scrape a doi."
  ;; remove http query part of the url
  (setq url (replace-regexp-in-string "?.*\\'" "" url))
  ;; remove anchor part of the url
  (setq url (replace-regexp-in-string "#.*\\'" "" url))
  (cond
   ;; Scitation
   ((string-match "https?://[a-z]+\\.scitation\\.org/doi/\\(?:pdf/\\)?\\(.+\\)" url)
    (url-unhex-string (match-string 1 url)))
   ;; Springer
   ((string-match "https?://link\\.springer\\.com/article/\\(.+\\)" url)
    (url-unhex-string (match-string 1 url)))
   ;; APS
   ((string-match "https?://journals\\.aps\\.org/[a-z]+/\\(?:abstract\\|pdf\\)/\\(.+\\)" url)
    (url-unhex-string (match-string 1 url)))
   ;; Nature
   ((string-match "https?://www\\.nature\\.com/articles/\\(.+\\)" url)
    (concat "10.1038/" (url-unhex-string (match-string 1 url))))
   ;; IOP
   ((string-match "https://iopscience.iop.org/article/\\(.+?\\)\\(?:/meta\\)?$" url)
    (url-unhex-string (match-string 1 url)))
   ))

(defun bibtex-capture-entry (&optional key)
  "Return bibtex entry for KEY.
Recognized KEYs are arXiv IDs, arXiv URLs, DOIs, journal URLs."
  (interactive)
  (let ((key (or key (completing-read "Key: " '("arXiv:" "doi:" "https:")))))
    (with-temp-buffer
      (cond
       ;; arXiv
       ((or (string-match "^arxiv:\\(.+\\)" key)
	    (string-match "^https?://arxiv\\.org/abs/\\(.+\\)" key)
	    (string-match "^https?://arxiv\\.org/pdf/\\(.+\\)\\.pdf" key))
	(insert (arxiv-get-bibtex-entry-via-arxiv-api (match-string 1 key))))
       ;; DOI
       ((or (string-match "^doi:\s*\\(.+\\)" key)
	    (string-match "^https?://doi\\.org/\\(.+\\)" key))
	(insert (doi-utils-doi-to-bibtex-string (match-string 1 key))))
       ;; DOI without prefix. do all DOIs start with "10."?
       ((string-match "^10\\." key)
	(insert (doi-utils-doi-to-bibtex-string key)))       
       ;; article URL
       ((string-match "^https?:" key)
	(let ((doi (or (url-to-doi key) (car (org-ref-url-scrape-dois key)))))
	  (when doi
	    (insert (doi-utils-doi-to-bibtex-string doi))))))
      (bibtex-mode)
      (org-ref-clean-bibtex-entry)
      (buffer-string))))

(defconst bibtex-keywords-re ",[ \t\n]*keywords[ \t\n]*=[ \t\n]*{\\(.+\\)}")

(defun bibtex-get-buffer-keywords ()
  (save-excursion
    (save-restriction
      (widen)
      (goto-char (point-min))
      (let ((keywords (make-hash-table :test 'equal)))
	(while (re-search-forward bibtex-keywords-re nil t)
	  (mapc (lambda (k) (puthash (s-trim k) nil keywords))
		(s-split "," (match-string-no-properties 1))))
	(sort (hash-table-keys keywords) #'string<)))))

(defun bibtex-normalize-keywords (keywords)
  (s-join ", " (mapcar #'s-trim (s-split "," keywords))))

(defmacro with-define-key (keymap key def &rest body)
  (declare (indent 3) (debug t))
  `(let ((map (make-sparse-keymap)))
     (define-key map ,key ,def)
     (let ((,keymap (make-composed-keymap map ,keymap)))
       (progn ,@body))))

(defun bibtex-read-keywords (current)
  ;; allow to insert spaces
  (with-define-key crm-local-completion-map (kbd "SPC") nil
    (completing-read-multiple "Keywords: " (bibtex-get-buffer-keywords) nil nil current)))

(defun bibtex-set-keywords (&optional keywords)
  (interactive)
  (bibtex-beginning-of-entry)
  (let* ((found (bibtex-search-forward-field "keywords" t))
	 ;; the current set of keywords for this entry
	 (value (and found (bibtex-text-in-field-bounds found t)))
	 (current (and value (bibtex-normalize-keywords value))))
    (if (null keywords) (setq keywords (bibtex-read-keywords current)))
    ;; delete old field
    (when found
      (goto-char (car (cdr found)))
      (bibtex-kill-field))
    ;; always add keywords as last field in the bibtex entry
    (bibtex-end-of-entry)
    (forward-line -1)
    (bibtex-make-field (list "keywords" nil (s-join ", " keywords) nil) t nil nil)
    ;; move pointer inside field delimiters
    (backward-char)))

(defun arxiv-get-bibtex-entry-pdf ()
  "Equivalent to `doi-utils-get-bibtex-entry-pdf' for arXiv entries." 
  (interactive "P")
  (save-excursion
    (bibtex-beginning-of-entry)
    (let* ((arxiv-number (bibtex-autokey-get-field "eprint"))
	   (key (cdr (assoc "=key=" (bibtex-parse-entry))))
	   (pdf-file (concat (file-name-as-directory org-ref-pdf-directory) key ".pdf"))
	   (pdf-url (concat "http://arxiv.org/pdf/" arxiv-number ".pdf")))
      ;; get file if needed
      (unless (file-exists-p pdf-file)
	(url-copy-file pdf-url pdf-file))
      (when (and doi-utils-open-pdf-after-download (file-exists-p pdf-file))
	(org-open-file pdf-file)))))
  
(defun bibtex-get-entry-pdf (&optional arg)
  (interactive "P")
  (save-excursion
    (bibtex-beginning-of-entry)
    (if arg
	;; ask for file name and copy it at the right path
	(let ((key (cdr (assoc "=key=" (bibtex-parse-entry)))))
	  (message (concat (file-name-as-directory org-ref-pdf-directory) key ".pdf"))
	  (copy-file (expand-file-name (read-file-name "PDF: " nil nil t))
		     (concat (file-name-as-directory org-ref-pdf-directory) key ".pdf")))
      (cond
       ;; DOI
       ((bibtex-search-forward-field "doi" t)
	(doi-utils-get-bibtex-entry-pdf))
       ;; arXiv
       ((let ((bounds (bibtex-search-forward-field "archivePrefix" t)))
	  (and bounds (string-equal (bibtex-text-in-field-bounds bounds t) "arXiv")))
	(arxiv-get-bibtex-entry-pdf))
       ;; fallback
       (t
	(message "Don't know how to download a PDF for this entry."))))))

(define-key bibtex-mode-map (kbd "C-c C-q") 'bibtex-set-keywords)
(define-key bibtex-mode-map (kbd "C-c C-p") 'bibtex-get-entry-pdf)
(define-key bibtex-mode-map (kbd "C-c C-o") 'org-ref-open-bibtex-pdf)

(provide 'bibtex-x)
